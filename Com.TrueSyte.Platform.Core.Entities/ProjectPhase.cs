﻿// <copyright file="ProjectPhase.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Project phases
    /// </summary>
    public enum ProjectPhase
    {
        /// <summary>
        /// Project conception and initiation
        /// </summary>
        [Display(Name = "Phase 1 - Project conception and initiation")]
        Initiation = 1,

        /// <summary>
        /// Project definition and planning
        /// </summary>
        [Display(Name = "Phase 2 - Project definition and planning")]
        Planning = 2,

        /// <summary>
        /// Project development/build execution
        /// </summary>
        [Display(Name = "Phase 3 - Project development/build execution")]
        Execution = 3,

        /// <summary>
        /// Project complete
        /// </summary>
        [Display(Name = "Phase 4 - Project complete")]
        CloseOut = 4,

        /// <summary>
        /// Project support/operation phase
        /// </summary>
        [Display(Name = "Phase 5 - Project support/operation phase")]
        Support = 5,

        /// <summary>
        /// Project archived
        /// </summary>
        [Display(Name = "Phase 6 - Project archived")]
        Archive = 6
    }
}

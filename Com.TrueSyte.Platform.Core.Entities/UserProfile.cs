﻿// <copyright file="UserProfile.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.Serialization;

    /// <summary>
    /// User profile base class
    /// </summary>
    [DataContract]
    public abstract class UserProfile
    {
        /// <summary>
        /// First name of the user
        /// </summary>
        private string firstName;

        /// <summary>
        /// Middle name of the user
        /// </summary>
        private string middleName;

        /// <summary>
        /// Last name of the user
        /// </summary>
        private string lastName;

        /// <summary>
        /// Nick name of the user
        /// </summary>
        private string nickName;

        /// <summary>
        /// Job title of the user
        /// </summary>
        private string jobTitle;

        /// <summary>
        /// Department at work
        /// </summary>
        private string department;
        
        /// <summary>
        /// Company of the user
        /// </summary>
        private string company;

        /// <summary>
        /// Street address
        /// </summary>
        private string street;

        /// <summary>
        /// City address
        /// </summary>
        private string city;

        /// <summary>
        /// State address
        /// </summary>
        private string state;

        /// <summary>
        /// Country address
        /// </summary>
        private string country;

        /// <summary>
        /// Home number
        /// </summary>
        private string homeNumber;

        /// <summary>
        /// Mobile number
        /// </summary>
        private string mobileNumber;

        /// <summary>
        /// Work number
        /// </summary>
        private string workNumber;

        /// <summary>
        /// Pager number
        /// </summary>
        private string pagerNumber;

        /// <summary>
        /// Fax number
        /// </summary>
        private string faxNumber;

        /// <summary>
        /// Voice over IP number
        /// </summary>
        private string voipNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserProfile" /> class
        /// </summary>
        public UserProfile()
        {
            this.Birthday = DateTime.UtcNow;
            this.City = string.Empty;
            this.Company = string.Empty;
            this.Country = string.Empty;
            this.Department = string.Empty;
            this.FaxNumber = string.Empty;
            this.FirstName = string.Empty;
            this.HomeNumber = string.Empty;
            this.JobTitle = string.Empty;
            this.LastName = string.Empty;
            this.MiddleName = string.Empty;
            this.MobileNumber = string.Empty;
            this.Nickname = string.Empty;
            this.PagerNumber = string.Empty;
            this.State = string.Empty;
            this.Street = string.Empty;
            this.VoIPNumber = string.Empty;
            this.WorkNumber = string.Empty;
        }

        /// <summary>
        /// Gets or sets the first name
        /// </summary>
        [DataMember]
        public virtual string FirstName
        {
            get { return this.firstName; }
            set { this.firstName = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the middle name
        /// </summary>
        [DataMember]
        public virtual string MiddleName
        {
            get { return this.middleName; }
            set { this.middleName = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the middle initial
        /// </summary>
        [DataMember]
        [NotMapped]
        public virtual char MiddleInitial
        {
            get
            {
                return string.IsNullOrEmpty(this.MiddleName) ? '\0' : this.MiddleName[0];
            }

            set 
            { 
            }
        }

        /// <summary>
        /// Gets or sets the last name
        /// </summary>
        [DataMember]
        public virtual string LastName
        {
            get { return this.lastName; }
            set { this.lastName = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the full name of a user
        /// </summary>
        [DataMember]
        [NotMapped]
        public virtual string Fullname
        {
            get
            {
                return string.Format("{0} {1}. {2}", this.FirstName, this.MiddleInitial, this.LastName);
            }

            set 
            { 
            }
        }

        /// <summary>
        /// Gets or sets the initials of a user
        /// </summary>
        [DataMember]
        [NotMapped]
        public virtual string Initials
        {
            get
            {
                string firstInitial = string.IsNullOrEmpty(this.FirstName) ? string.Empty : this.FirstName[0].ToString();
                string lastInitial = string.IsNullOrEmpty(this.LastName) ? string.Empty : this.LastName[0].ToString();

                return string.Format("{0}{1}{2}", this.FirstName, this.MiddleInitial, this.LastName);
            }

            set 
            { 
            }
        }

        /// <summary>
        /// Gets or sets the nick name
        /// </summary>
        [DataMember]
        public virtual string Nickname
        {
            get { return this.nickName; }
            set { this.nickName = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the birthday
        /// </summary>
        [DataMember]
        public virtual DateTime Birthday { get; set; }

        /// <summary>
        /// Gets or sets the job title
        /// </summary>
        [DataMember]
        public virtual string JobTitle
        {
            get { return this.jobTitle; }
            set { this.jobTitle = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the department
        /// </summary>
        [DataMember]
        public virtual string Department
        {
            get { return this.department; }
            set { this.department = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the company
        /// </summary>
        [DataMember]
        public virtual string Company
        {
            get { return this.company; }
            set { this.company = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the street
        /// </summary>
        [DataMember]
        public virtual string Street
        {
            get { return this.street; }
            set { this.street = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the city
        /// </summary>
        [DataMember]
        public virtual string City
        {
            get { return this.city; }
            set { this.city = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the state
        /// </summary>
        [DataMember]
        public virtual string State
        {
            get { return this.state; }
            set { this.state = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the postal code
        /// </summary>
        [DataMember]
        public virtual int PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the country
        /// </summary>
        [DataMember]
        public virtual string Country
        {
            get { return this.country; }
            set { this.country = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the complete address
        /// </summary>
        [DataMember]
        [NotMapped]
        public virtual string Address
        {
            get
            {
                return string.Format("{0}, {1}, {2} {3}, {4}", this.Street, this.City, this.State, this.PostalCode, this.Country);
            }

            set 
            { 
            }
        }

        /// <summary>
        /// Gets or sets the home number
        /// </summary>
        [DataMember]
        public virtual string HomeNumber
        {
            get { return this.homeNumber; }
            set { this.homeNumber = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the mobile number
        /// </summary>
        [DataMember]
        public virtual string MobileNumber
        {
            get { return this.mobileNumber; }
            set { this.mobileNumber = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the work number
        /// </summary>
        [DataMember]
        public virtual string WorkNumber
        {
            get { return this.workNumber; }
            set { this.workNumber = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the pager number
        /// </summary>
        [DataMember]
        public virtual string PagerNumber
        {
            get { return this.pagerNumber; }
            set { this.pagerNumber = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the fax number
        /// </summary>
        [DataMember]
        public virtual string FaxNumber
        {
            get { return this.faxNumber; }
            set { this.faxNumber = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the Voice of IP number
        /// </summary>
        [DataMember]
        public virtual string VoIPNumber
        {
            get { return this.voipNumber; }
            set { this.voipNumber = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }
    }
}

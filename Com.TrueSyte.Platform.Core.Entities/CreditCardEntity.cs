﻿// <copyright file="CreditCardEntity.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.Serialization;

    /// <summary>
    /// Credit card entity class
    /// </summary>
    [DataContract]
    [Table("CreditCards", Schema = "truesyte")]
    public class CreditCardEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditCardEntity" /> class
        /// </summary>
        public CreditCardEntity()
        {
            this.CardType = CreditCardType.VISA;
            this.ExpirationMonth = 01;
            this.ExpirationYear = DateTime.UtcNow.Year;
            this.IsPrimary = false;
            this.Name = string.Empty;
            this.Number = string.Empty;
            this.SecurityCode = 000;
        }

        /// <summary>
        /// Gets or sets the primary key
        /// </summary>
        [DataMember]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int CreditCardId { get; set; }

        /// <summary>
        /// Gets or sets the type of card
        /// </summary>
        [DataMember]
        public CreditCardType CardType { get; set; }

        /// <summary>
        /// Gets or sets the card name
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the card number
        /// </summary>
        [DataMember]
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the 3 digit security code of the card
        /// </summary>
        [DataMember]
        public int SecurityCode { get; set; }

        /// <summary>
        /// Gets or sets the 2 digit expiration month
        /// </summary>
        [DataMember]
        public int ExpirationMonth { get; set; }

        /// <summary>
        /// Gets or sets the 4 digit expiration year
        /// </summary>
        [DataMember]
        public int ExpirationYear { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this credit card is set as the primary
        /// </summary>
        [DataMember]
        public bool IsPrimary { get; set; }

        /// <summary>
        /// Gets or sets the associated primary key of the owner
        /// </summary>
        [DataMember]
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the <c>UserEntity</c> entity
        /// </summary>
        [ForeignKey("UserId")]
        public virtual UserEntity User { get; set; }
    }
}

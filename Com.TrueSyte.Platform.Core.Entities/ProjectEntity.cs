﻿// <copyright file="ProjectEntity.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.Serialization;

    /// <summary>
    /// Project entity class
    /// </summary>
    [DataContract]
    [Table("Projects", Schema = "truesyte")]
    public class ProjectEntity
    {
        /// <summary>
        /// Project title
        /// </summary>
        private string title;

        /// <summary>
        /// Project subtitle
        /// </summary>
        private string subtitle;

        /// <summary>
        /// Project description
        /// </summary>
        private string description;

        /// <summary>
        /// Project purpose
        /// </summary>
        private string purpose;

        /// <summary>
        /// Project objectives
        /// </summary>
        private string objectives;

        /// <summary>
        /// Project technologies
        /// </summary>
        private string technologies;

        /// <summary>
        /// Project dependencies
        /// </summary>
        private string dependencies;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectEntity" /> class
        /// </summary>
        public ProjectEntity()
        {
            DateTime currentDate = DateTime.Now;

            this.ActualEndDate = currentDate;
            this.ActualStartDate = currentDate;
            this.CreatedDate = currentDate;
            this.Dependencies = string.Empty;
            this.Description = string.Empty;
            this.EstimatedCost = 0.00M;
            this.EstimatedEndDate = currentDate;
            this.EstimatedStartDate = currentDate;
            this.ModifiedDate = currentDate;
            this.Objectives = string.Empty;
            this.Phase = ProjectPhase.Initiation;
            this.PlannedBudget = 0.00M;
            this.PlannedEndDate = currentDate;
            this.PlannedStartDate = currentDate;
            this.Purpose = string.Empty;
            this.Status = ProjectStatus.Draft;
            this.Subtitle = string.Empty;
            this.Title = string.Empty;
        }

        /// <summary>
        /// Gets or sets the primary key
        /// </summary>
        [DataMember]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ProjectId { get; set; }

        /// <summary>
        /// Gets or sets the project title
        /// </summary>
        [DataMember]
        public string Title
        {
            get { return this.title; }
            set { this.title = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the project subtitle
        /// </summary>
        [DataMember]
        public string Subtitle
        {
            get { return this.subtitle; }
            set { this.subtitle = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the project description
        /// </summary>
        [DataMember]
        public string Description
        {
            get { return this.description; }
            set { this.description = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the purpose of the project
        /// </summary>
        [DataMember]
        public string Purpose
        {
            get { return this.purpose; }
            set { this.purpose = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the objectives of the project
        /// </summary>
        [DataMember]
        public string Objectives
        {
            get { return this.objectives; }
            set { this.objectives = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the technologies 
        /// </summary>
        [DataMember]
        public string Technologies
        {
            get { return this.technologies; }
            set { this.technologies = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the dependencies of the project
        /// </summary>
        [DataMember]
        public string Dependencies
        {
            get { return this.dependencies; }
            set { this.dependencies = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the planned budget
        /// </summary>
        [DataMember]
        public decimal PlannedBudget { get; set; }

        /// <summary>
        /// Gets or sets the estimated cost
        /// </summary>
        [DataMember]
        public decimal EstimatedCost { get; set; }

        /// <summary>
        /// Gets or sets the actual cost
        /// </summary>
        [DataMember]
        public decimal ActualCost { get; set; }

        /// <summary>
        /// Gets or sets the current phase of the project
        /// </summary>
        [DataMember]
        public ProjectPhase Phase { get; set; }

        /// <summary>
        /// Gets or sets the current status in the project phase
        /// </summary>
        [DataMember]
        public ProjectStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the planned start date
        /// </summary>
        [DataMember]
        public DateTime PlannedStartDate { get; set; }

        /// <summary>
        /// Gets or sets the planned end date
        /// </summary>
        [DataMember]
        public DateTime PlannedEndDate { get; set; }

        /// <summary>
        /// Gets or sets the estimated start date
        /// </summary>
        [DataMember]
        public DateTime EstimatedStartDate { get; set; }

        /// <summary>
        /// Gets or sets the estimated end date
        /// </summary>
        [DataMember]
        public DateTime EstimatedEndDate { get; set; }

        /// <summary>
        /// Gets or sets the actual start date
        /// </summary>
        [DataMember]
        public DateTime ActualStartDate { get; set; }

        /// <summary>
        /// Gets or sets the actual end date
        /// </summary>
        [DataMember]
        public DateTime ActualEndDate { get; set; }

        /// <summary>
        /// Gets or sets the project creation date
        /// </summary>
        [DataMember]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the projects modified date
        /// </summary>
        [DataMember]
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the associated primary key of the owner
        /// </summary>
        [DataMember]
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the <c>UserEntity</c> entity
        /// </summary>
        [ForeignKey("UserId")]
        public virtual UserEntity User { get; set; }

        /// <summary>
        /// Gets or sets the associated primary key of the lead expert
        /// </summary>
        [DataMember]
        public int LeadExpertId { get; set; }

        /// <summary>
        /// Gets or sets the <c>ExpertEntity</c> entity
        /// </summary>
        [ForeignKey("LeadExpertId")]
        public virtual ExpertEntity LeadExpert { get; set; }

        /// <summary>
        /// Gets or sets the list of <c>ExpertEntity</c> entity
        /// </summary>
        public virtual ICollection<ExpertEntity> Experts { get; set; }
    }
}

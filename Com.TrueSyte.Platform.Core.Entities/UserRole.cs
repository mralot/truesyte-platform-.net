﻿// <copyright file="UserRole.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    /// <summary>
    /// User roles
    /// </summary>
    public enum UserRole
    {
        /// <summary>
        /// Project owner role
        /// </summary>
        ProjectOwner = 0,

        /// <summary>
        /// Expert role
        /// </summary>
        Expert = 1,

        /// <summary>
        /// Lead Expert role
        /// </summary>
        LeadExpert = 2,

        /// <summary>
        /// Scrum Master role
        /// </summary>
        ScrumMaster = 3,

        /// <summary>
        /// Administrator role
        /// </summary>
        Administrator = 4,

        /// <summary>
        /// System Administrator role
        /// </summary>
        SystemAdministrator = 5
    }
}

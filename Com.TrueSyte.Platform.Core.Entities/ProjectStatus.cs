﻿// <copyright file="ProjectStatus.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Project status
    /// </summary>
    public enum ProjectStatus
    {
        /// <summary>
        /// Draft Status
        /// </summary>
        [Display(Name = "Draft")]
        Draft = 1,

        /// <summary>
        /// In Progress Status
        /// </summary>
        [Display(Name = "In Progress")]
        InProgress = 2,

        /// <summary>
        /// For Review (Lead Expert) Status
        /// </summary>
        [Display(Name = "For Review (Lead Expert)")]
        ForLeadExpertReview = 3,

        /// <summary>
        /// In Progress (Lead Expert) Status
        /// </summary>
        [Display(Name = "In Progress (Lead Expert)")]
        LeadExpertInProgress = 4,

        /// <summary>
        /// For Review Status
        /// </summary>
        [Display(Name = "For Review")]
        ForReview = 5,

        /// <summary>
        /// Rework Status
        /// </summary>
        [Display(Name = "Rework")]
        Rework = 6,

        /// <summary>
        /// Completed Status
        /// </summary>
        [Display(Name = "Completed")]
        Completed = 7
    }
}

﻿// <copyright file="CreditCardType.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Types of credit card
    /// </summary>
    public enum CreditCardType
    {
        /// <summary>
        /// MasterCard card
        /// </summary>
        [Display(Name = "MasterCard")]
        MasterCard = 1,

        /// <summary>
        /// VISA card
        /// </summary>
        [Display(Name = "VISA")]
        VISA = 2
    }
}

﻿// <copyright file="UserEntity.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.Serialization;

    /// <summary>
    /// User entity class
    /// </summary>
    [DataContract]
    [Table("Users", Schema = "truesyte")]
    public class UserEntity : UserProfile
    {
        /// <summary>
        /// E-mail address
        /// </summary>
        private string emailAddress;

        /// <summary>
        /// Salt for the password
        /// </summary>
        private string salt;

        /// <summary>
        /// Encrypted password
        /// </summary>
        private string password;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserEntity" /> class
        /// </summary>
        public UserEntity() :
            base()
        {
            this.EmailAddress = string.Empty;
            this.IsActive = true;
            this.IsAuthenticated = false;
            this.IsEmailVerified = false;
            this.IsLocked = false;
            this.IsPasswordSystemGenerated = true;
            this.LastPasswordChange = DateTime.UtcNow;
            this.LastSuccessfulLogin = DateTime.UtcNow;
            this.Password = string.Empty;
            this.Salt = string.Empty;
            this.UserRoles = new List<UserRole>() { UserRole.ProjectOwner };
        }

        /// <summary>
        /// Gets or sets the primary key
        /// </summary>
        [DataMember]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets different user roles
        /// </summary>
        [DataMember]
        public IList<UserRole> UserRoles { get; set; }  

        /// <summary>
        /// Gets or sets the e-mail address
        /// </summary>
        [DataMember]
        public string EmailAddress
        {
            get { return this.emailAddress; }
            set { this.emailAddress = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the salt
        /// </summary>
        [DataMember]
        public string Salt
        {
            get { return this.salt; }
            set { this.salt = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the password
        /// </summary>
        [DataMember]
        public string Password
        {
            get { return this.password; }
            set { this.password = string.IsNullOrEmpty(value) ? string.Empty : value; }
        }

        /// <summary>
        /// Gets or sets the last successful login
        /// </summary>
        [DataMember]
        public DateTime LastSuccessfulLogin { get; set; }

        /// <summary>
        /// Gets or sets the date of the last password change
        /// </summary>
        [DataMember]
        public DateTime LastPasswordChange { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the e-mail address is verified
        /// </summary>
        [DataMember]
        public bool IsEmailVerified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the password is system generated
        /// </summary>
        [DataMember]
        public bool IsPasswordSystemGenerated { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user is active
        /// </summary>
        [DataMember]
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the account is locked
        /// </summary>
        [DataMember]
        public bool IsLocked { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user is authenticated
        /// </summary>
        [NotMapped]
        public bool IsAuthenticated { get; set; }
    }
}

﻿// <copyright file="ExpertEntity.cs" company="TrueSyte Technologies Inc.">
//     Copyright (c) TrueSyte Technologies Inc. All rights reserved.
// </copyright>

namespace Com.TrueSyte.Platform.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.Serialization;

    /// <summary>
    /// Expert entity class
    /// </summary>
    [DataContract]
    [Table("Experts", Schema = "truesyte")]
    public class ExpertEntity : UserProfile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExpertEntity" /> class
        /// </summary>
        public ExpertEntity()
            : base()
        {
            // No logic
        }

        /// <summary>
        /// Gets or sets the primary key
        /// </summary>
        [DataMember]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ExpertId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the expert is a lead expert
        /// </summary>
        [DataMember]
        public bool IsLeadExpert { get; set; }

        /// <summary>
        /// Gets or sets the e-mail address
        /// </summary>
        [DataMember]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the skills
        /// </summary>
        [DataMember]
        public string Skills { get; set; }

        /// <summary>
        /// Gets or sets the start date
        /// </summary>
        [DataMember]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date
        /// </summary>
        [DataMember]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the expert is still active
        /// </summary>
        [DataMember]
        public bool IsActive { get; set; }
    }
}

﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<System.DateTime?>" %>
<%: Html.TextBox(
        string.Empty,
        Model.HasValue ? Model.Value.ToString("MM/dd/yyyy") : string.Empty
    )
%>


﻿using Com.TrueSyte.Platform.Core;
using Com.TrueSyte.Platform.Core.Models;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace Com.TrueSyte.Platform.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class ProjectController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Confirmation()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Dashboard()
        {
            ProjectDashboardModel model = new ProjectDashboardModel();

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterProjectModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string emailAddress = HttpContext.User.Identity.Name;
                    bool result =
                        ServiceLocator.Instance.GetService<IPlatformService>().RegisterProject(emailAddress, model);

                    if (result)
                    {
                        return RedirectToAction("Confirmation", "Project");
                    }
                }
                catch (SqlException ex)
                {
                    ModelState.AddModelError("ProjectController.RegisterSqlException", ex.Message);
                }
            }

            return View();
        }

    }
}

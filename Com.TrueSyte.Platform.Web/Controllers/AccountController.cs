﻿namespace Com.TrueSyte.Platform.Web.Controllers
{
    using System;
    using System.Data.SqlClient;
    using System.Web.Mvc;
    using System.Web.Security;
    using Com.TrueSyte.Platform.Core;
    using Com.TrueSyte.Platform.Core.Entities;
    using Com.TrueSyte.Platform.Core.Models;
    using Com.TrueSyte.Platform.Core.PersistentStorage;

    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Default()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult LogOn(Uri returnUrl)
        {
            if (returnUrl == null)
            {
                throw new ArgumentNullException("returnUrl");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LogOn(LogOnModel model, Uri returnUrl)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            if (returnUrl == null)
            {
                throw new ArgumentNullException("returnUrl");
            }

            if (ModelState.IsValid)
            {
                UserEntity user = 
                    ServiceLocator.Instance.GetService<IPlatformService>().LogOn(model);

                if (user.IsAuthenticated)
                {
                    FormsAuthentication.SetAuthCookie(user.EmailAddress, createPersistentCookie: true);

                    if (user.IsPasswordSystemGenerated)
                    {
                        return RedirectToAction("ManagePassword", "Account");
                    }
                    
                    return RedirectToAction("Dashboard", "Home");
                }

                return RedirectToAction("Default", "Home");
            }

            ModelState.AddModelError("AccountController.LoginError", "The user name or password provided is incorrect.");

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Default", "Home");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterUserModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    bool result =
                        ServiceLocator.Instance.GetService<IPlatformService>().RegisterUser(model);

                    if (result)
                    {
                        return RedirectToAction("Confirmation", "Account");
                    }
                }
                catch (SqlException ex)
                {
                    ModelState.AddModelError("AccountController.RegisterSqlException", ex.Message);
                }
                catch (UniqueConstraintException ex)
                {
                    ModelState.AddModelError("AccountController.RegisterUniqueConstraintException", ex.Message);
                }
            }

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Confirmation()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ManagePassword()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManagePassword(ManagePasswordModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string emailAddress = HttpContext.User.Identity.Name;
                    bool result = 
                        ServiceLocator.Instance.GetService<IPlatformService>().ManagePassword(emailAddress, model);

                    if (result)
                    {
                        // TODO: Display to the user that the change of password is successful.

                        return RedirectToAction("Confirmation", "Account");
                    }
                }
                catch (SqlException ex)
                {
                    ModelState.AddModelError("AccountController.ManagePasswordSqlException", ex.Message);
                }
            }

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageUser()
        {
            string emailAddress = HttpContext.User.Identity.Name;
            ManageUserModel model = 
                ServiceLocator.Instance.GetService<IPlatformService>().GetUserDetails(emailAddress);

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageUser(ManageUserModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    string emailAddress = HttpContext.User.Identity.Name;
                    bool result = 
                        ServiceLocator.Instance.GetService<IPlatformService>().ManageUser(emailAddress, model);

                    if (result)
                    {
                        return RedirectToAction("Confirmation", "Account");
                    }
                }
                catch (SqlException ex)
                {
                    ModelState.AddModelError("AccountController.ManageUserSqlException", ex.Message);
                }
            }

            ModelState.AddModelError("AccountController.ManageUserError", "The user name or password provided is incorrect.");

            return View();
        }
    }
}

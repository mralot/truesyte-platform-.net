﻿namespace Com.TrueSyte.Platform.Web
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// 
    /// </summary>
    public static class RouteConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            if (routes == null)
            {
                throw new ArgumentNullException("routes");
            }

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Home", action = "Default", id = UrlParameter.Optional });
        }
    }
}
﻿namespace Com.TrueSyte.Platform.Web
{
    using System;
    using System.Web.Optimization;

    /// <summary>
    /// 
    /// </summary>
    public static class BundleConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bundles"></param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            if (bundles == null)
            {
                throw new ArgumentNullException("bundles");
            }

            bundles.Add(new StyleBundle("~/content/corestyle").Include(
                "~/content/themes/base/fonts.css",
                "~/content/themes/base/bootstrap.css",
                "~/content/themes/base/font-awesome.css",
                "~/content/themes/base/prettyPhoto.css",
                "~/content/themes/base/isotope.css",
                "~/content/themes/base/settings.css",
                "~/content/themes/base/theme.css",
                "~/content/themes/base/bootstrap-responsive.css",
                "~/content/themes/base/theme-responsive.css",
                "~/content/themes/base/mode-slash.css",
                "~/content/themes/base/color-blue.css",
                "~/content/themes/base/jquery-ui.css",
                "~/content/com.truesyte.core.css")
            );

            bundles.Add(new ScriptBundle("~/bundles/corescript").Include(
                "~/scripts/modernizr.js",
                "~/scripts/jquery-1.10.2.js",
                "~/scripts/bootstrap.js",
                "~/scripts/retina.js",
                "~/scripts/tinyscrollbar.js",
                "~/scripts/caroufredsel.js",
                "~/scripts/plugins.js",
                "~/scripts/jquery.prettyPhoto.js",
                "~/scripts/jquery.isotope.js",
                "~/scripts/jquery.tweet.js",
                "~/scripts/jquery-ui-1.10.3.custom.js",
                "~/scripts/jquery.maskedinput-1.3.1.js",
                "~/scripts/jquery.maskMoney.js",
                "~/scripts/theme.js",
                "~/scripts/com.truesyte.core.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/scripts/jquery.unobtrusive*",
                "~/scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jsthemes").Include(
                "~/scripts/jquery.themepunch.plugins.js",
                "~/scripts/jquery.themepunch.revolution.js")
            );
        }
    }
}
﻿var Com = {};
Com.TrueSyte = function (args) {
    var _this = this;

    _this.constructor = function () {
        var dateFormat = "mm/dd/yy";
        var creditCardDateFormat = "99/9999";
        var phoneFormat = "+99 (999) 999-9999?";
        
        $("#Birthday").datepicker({ dateFormat: dateFormat });
        $("#PlannedStartDate").datepicker({ dateFormat: dateFormat });
        $("#PlannedEndDate").datepicker({ dateFormat: dateFormat });

        $("#HomeNumber").mask(phoneFormat);
        $("#MobileNumber").mask(phoneFormat);
        $("#FaxNumber").mask(phoneFormat);
        $("#PagerNumber").mask(phoneFormat);
        $("#WorkNumber").mask(phoneFormat);
        $("#VoIPNumber").mask(phoneFormat);

        $("#ExpireationDate").mask(creditCardDateFormat);

        $("#PlannedBudget").maskMoney({ prefix: '$ ', allowNegative: false, thousands: ',', decimal: '.', affixesStay: false });
    };

    _this.constructor();
};

Com.TrueSyte.InitializeWizardForm = function (args) {
    var _this = this;

    _this.submitButtonName = args.submitButtonName;

    var InitializeBackButton = function (i) {
        var stepName = "step" + i;
        $("#" + stepName + "commands").append("<input id='" + stepName + "Prev' type='button' value='< Back' />");
        $("#" + stepName + "Prev").bind("click", function (e) {
            $("#" + stepName).hide();
            $("#step" + (i - 1)).show();
            ToggleStep(i - 1);
        });
    };

    var InitializeNextButton = function (i) {
        var stepName = "step" + i;
        $("#" + stepName + "commands").append("<input id='" + stepName + "Next' type='button' value='Next >' />");
        $("#" + stepName + "Next").bind("click", function (e) {
            $("#" + stepName).hide();
            $("#step" + (i + 1)).show();
            ToggleStep(i + 1);
        });
    };

    var InitializeSubmitButton = function (i) {
        var stepName = "step" + i;
        $("#" + stepName + "commands").append("<input type='submit' value='" + _this.submitButtonName + "' />");
    };

    var ToggleStep = function (i) {
        $("#steps li").removeClass("current");
        $("#stepDesc" + i).addClass("current");
    };

    var InitializeComponents = function () {
        $(".vc_content form").before("<ul id='steps'></ul>");

        var count = $(".vc_content form fieldset").size();

        $(".vc_content form fieldset").each(function (i, e) {
            var _this = this;

            $(_this).wrap("<div id='step" + i + "'></div>");
            $(_this).find("ul").append("<li id='step" + i + "commands' style='margin-right: 8px;'></li>");

            var name = $(_this).find("input[type='hidden']").val();
            $("#steps").append("<li id='stepDesc" + i + "'>Step " + (i + 1) + "<span>" + name + "</span></li>");

            if (i == 0) {
                InitializeNextButton(i);
                ToggleStep(i);
            }
            else if (i == (count -1)) {
                $("#step" + i).hide();
                InitializeBackButton(i);
                InitializeSubmitButton(i);
            }
            else {
                $("#step" + i).hide();
                InitializeBackButton(i);
                InitializeNextButton(i);
            }
        });
    };

    InitializeComponents();
};

$(document).ready(function () {
    var truesyte = new Com.TrueSyte();
});


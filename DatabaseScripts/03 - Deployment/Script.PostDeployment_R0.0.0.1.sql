USE [PlatformData]
GO

SET IDENTITY_INSERT [truesyte].[Experts] ON
INSERT INTO [truesyte].[Experts] ([ExpertId],[EmailAddress],[IsLeadExpert],[Skills],[StartDate],[EndDate],[FirstName],[MiddleName],[LastName],[Nickname],[Gender],[Birthday],[JobTitle],[Department],[Company],[Street],[City],[State],[ZipCode],[Country],[HomeNumber],[MobileNumber],[WorkNumber],[PagerNumber],[FaxNumber],[VoIPNumber],[IsActive]) VALUES (-1, N'noexpert@truesyte.com', 1, N'Not applicable', GETDATE(), GETDATE(), N'Not applicable', N'Not applicable', N'Not applicable', N'Not applicable', 'M', GETDATE(), N'Not applicable', N'Not applicable', N'TrueSyte Technologies Inc.', N'Not applicable', N'Not applicable', N'Not applicable', 0, N'Philippines', N'63021234567', N'639171234567', N'', N'', N'', N'', 1);
SET IDENTITY_INSERT [truesyte].[Experts] OFF
GO


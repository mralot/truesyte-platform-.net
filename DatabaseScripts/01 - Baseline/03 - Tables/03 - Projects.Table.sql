USE [PlatformData]
GO

CREATE TABLE [truesyte].[Projects] (
	[ProjectId]				INT IDENTITY (1, 1) NOT NULL,
	[UserId]				INT NOT NULL,
	[LeadExpertId]			INT NOT NULL,
	[Status]				INT NOT NULL,
	[Phase]					INT NOT NULL,
	[Title]					NVARCHAR(200) NOT NULL,
	[Subtitle]				NVARCHAR(200) NOT NULL,
	[Description]			NVARCHAR(MAX) NOT NULL,
	[Purpose]				NVARCHAR(MAX) NOT NULL,
	[Objectives]			NVARCHAR(MAX) NOT NULL,
	[Technologies]			NVARCHAR(500) NOT NULL,
	[Dependencies]			NVARCHAR(500) NOT NULL,
	[PlannedBudget]			DECIMAL(18, 2) NOT NULL,
	[EstimatedCost]			DECIMAL(18, 2) NOT NULL,
	[ActualCost]			DECIMAL(18, 2) NOT NULL,
	[PlannedStartDate]		DATETIME NOT NULL,
	[PlannedEndDate]		DATETIME NOT NULL,
	[EstimatedStartDate]	DATETIME NOT NULL,
	[EstimatedEndDate]		DATETIME NOT NULL,
	[ActualStartDate]		DATETIME NOT NULL,
	[ActualEndDate]			DATETIME NOT NULL,
	[CreatedDate]			DATETIME NOT NULL,
	[ModifiedDate]			DATETIME NOT NULL
) ON [PRIMARY];
GO

ALTER TABLE [truesyte].[Projects]
    ADD CONSTRAINT [PK_Projects_ProjectId] PRIMARY KEY CLUSTERED ([ProjectId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);
GO

ALTER TABLE [truesyte].[Projects] WITH NOCHECK
    ADD CONSTRAINT [FK_Projects_UserId_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [truesyte].[Users] ([UserId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

ALTER TABLE [truesyte].[Projects] WITH NOCHECK
    ADD CONSTRAINT [FK_Projects_LeadExpertId_Experts_ExpertId] FOREIGN KEY ([LeadExpertId]) REFERENCES [truesyte].[Experts] ([ExpertId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO


USE [PlatformData]
GO

CREATE TABLE [truesyte].[Users] (
	[UserId]					INT IDENTITY (1, 1) NOT NULL,
	[EmailAddress]				NVARCHAR(200) NOT NULL,
	[Salt]						NVARCHAR(172) NOT NULL,
	[Password]					NVARCHAR(1848) NOT NULL,
	[FirstName]					NVARCHAR(100) NOT NULL,
	[MiddleName]				NVARCHAR(100) NOT NULL,
	[LastName]					NVARCHAR(100) NOT NULL,
	[Nickname]					NVARCHAR(100) NOT NULL,
	[Birthday]					DATETIME NOT NULL,
	[JobTitle]					NVARCHAR(100) NOT NULL,
	[Department]				NVARCHAR(100) NOT NULL,
	[Company]					NVARCHAR(100) NOT NULL,
	[Street]					NVARCHAR(100) NOT NULL,
	[City]						NVARCHAR(100) NOT NULL,
	[State]						NVARCHAR(100) NOT NULL,
	[PostalCode]				INT NOT NULL,
	[Country]					NVARCHAR(100) NOT NULL,
	[HomeNumber]				NVARCHAR(20) NOT NULL,
	[MobileNumber]				NVARCHAR(20) NOT NULL,
	[WorkNumber]				NVARCHAR(20) NOT NULL,
	[PagerNumber]				NVARCHAR(20) NOT NULL,
	[FaxNumber]					NVARCHAR(20) NOT NULL,
	[VoIPNumber]				NVARCHAR(20) NOT NULL,
	[LastSuccessfulLogin]		DATETIME NOT NULL,
	[LastPasswordChange]		DATETIME NOT NULL,
	[IsEmailVerified]			BIT NOT NULL,
	[IsPasswordSystemGenerated]	BIT NOT NULL,
	[IsLocked]					BIT NOT NULL,
	[IsActive]					BIT NOT NULL
) ON [PRIMARY];
GO

ALTER TABLE [truesyte].[Users]
    ADD CONSTRAINT [PK_Users_UserId] PRIMARY KEY CLUSTERED ([UserId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);
GO

ALTER TABLE [truesyte].[Users]
    ADD CONSTRAINT [UC_Users_EmailAddress] UNIQUE NONCLUSTERED ([EmailAddress] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);
GO


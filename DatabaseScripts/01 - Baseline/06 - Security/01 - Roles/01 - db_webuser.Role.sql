USE [PlatformData]
GO

CREATE ROLE [db_webuser] AUTHORIZATION [dbo]
GO

GRANT DELETE, INSERT, SELECT, UPDATE ON [truesyte].[Experts] TO [db_webuser]
GRANT DELETE, INSERT, SELECT, UPDATE ON [truesyte].[Projects] TO [db_webuser]
GRANT DELETE, INSERT, SELECT, UPDATE ON [truesyte].[Users] TO [db_webuser]
GO

EXEC sp_addrolemember @rolename=N'db_webuser', @membername=N'truesytewebuser';
GO


USE [master]
GO

IF  EXISTS (SELECT * FROM sys.server_principals WHERE name = N'truesytewebuser')
	DROP LOGIN [truesytewebuser]
GO

CREATE LOGIN [truesytewebuser] WITH PASSWORD=N'L!m!+3dw3bU$Er__', DEFAULT_DATABASE=[PlatformData], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=ON, CHECK_POLICY=ON
GO

ALTER LOGIN [truesytewebuser] DISABLE
GO

USE [PlatformData]
GO

IF EXISTS (SELECT * FROM sys.database_principals WHERE name = N'truesytewebuser')
	DROP USER [truesytewebuser]
GO

USE [PlatformData]
GO

CREATE USER [truesytewebuser] FOR LOGIN [truesytewebuser] WITH DEFAULT_SCHEMA=[dbo]
GO

USE [PlatformData]
GO

IF EXISTS (SELECT * FROM sys.schemas WHERE name = N'truesyte')
	DROP SCHEMA [truesyte]
GO

USE [PlatformData]
GO

CREATE SCHEMA [truesyte] AUTHORIZATION [dbo]
GO

USE [PlatformData]
GO

DECLARE @RoleName sysname
SET @RoleName = N'db_webuser'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	SELECT [name]
	FROM sys.database_principals 
	WHERE principal_id IN ( 
		SELECT member_principal_id 
		FROM sys.database_role_members 
		WHERE role_principal_id IN (
			SELECT principal_id
			FROM sys.database_principals WHERE [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXEC sp_droprolemember @rolename=@RoleName, @membername=@RoleMemberName

		FETCH NEXT FROM Member_Cursor
		INTO @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
GO

IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'db_webuser' AND type = 'R')
	DROP ROLE [db_webuser]
GO






﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core
{
    using Com.TrueSyte.Platform.Core.Entities;
    using Com.TrueSyte.Platform.Core.Models;

    /// <summary>
    /// 
    /// </summary>
    public interface IPlatformService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        UserEntity LogOn(LogOnModel model);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        ManageUserModel GetUserDetails(string emailAddress);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool RegisterUser(RegisterUserModel model);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        bool ManageUser(string emailAddress, ManageUserModel model);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        bool ManagePassword(string emailAddress, ManagePasswordModel model);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        bool RegisterProject(string emailAddress, RegisterProjectModel model);
    }
}

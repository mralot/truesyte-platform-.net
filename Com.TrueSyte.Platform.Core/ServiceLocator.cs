﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core
{
    using Com.TrueSyte.Platform.Core.Repositories;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public sealed class ServiceLocator : IServiceLocator
    {
        private IDictionary<object, object> _services;
        private static volatile ServiceLocator _instance;
        private static object _padlock = new object();

        /// <summary>
        /// 
        /// </summary>
        public static IServiceLocator Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_padlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new ServiceLocator();
                        }
                    }
                }

                return _instance;
            }
        }

        private ServiceLocator()
        {
            _services = new Dictionary<object, object>();

            _services.Add(typeof(IExpertRepository), new ExpertRepository());
            _services.Add(typeof(IProjectRepository), new ProjectRepository());
            _services.Add(typeof(IUserRepository), new UserRepository());
            _services.Add(typeof(IPlatformService), new PlatformService());
        }

        public T GetService<T>()
        {
            try
            {
                return (T)_services[typeof(T)];
            }
            catch (KeyNotFoundException)
            {
                throw new ApplicationException("The requested service is not registered");
            }
        }
    }
}

﻿using Com.TrueSyte.Platform.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.TrueSyte.Platform.Core.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class UserDashboardModel
    {
        UserEntity User { get; set; }
        IList<ProjectEntity> Projects { get; set; }
    }
}

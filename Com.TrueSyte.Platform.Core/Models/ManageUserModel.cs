﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Com.TrueSyte.Platform.Core.Models
{
    public class ManageUserModel
    {
        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = true)]
        [Display(Name = "Middle name")]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [UIHint("DatePickerTemplate")]
        [Display(Name = "Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Birthday { get; set; }

        [Display(Name = "Company")]
        public string Company { get; set; }

        [Display(Name = "Department")]
        public string Department { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Display(Name = "Street")]
        public string Street { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        [DataType(DataType.PostalCode)]
        [Display(Name = "Postal Code")]
        public int PostalCode { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Home Number")]
        public string HomeNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Work Number")]
        public string WorkNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Pager Number")]
        public string PagerNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Fax Number")]
        public string FaxNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "VoIP Number")]
        public string VoIPNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ManageUserModel()
        {
            Birthday = DateTime.UtcNow;
            City = string.Empty;
            Company = string.Empty;
            Country = string.Empty;
            Department = string.Empty;
            FaxNumber = string.Empty;
            FirstName = string.Empty;
            HomeNumber = string.Empty;
            JobTitle = string.Empty;
            LastName = string.Empty;
            MiddleName = string.Empty;
            MobileNumber = string.Empty;
            PagerNumber = string.Empty;
            PostalCode = 0;
            State = string.Empty;
            Street = string.Empty;
            VoIPNumber = string.Empty;
            WorkNumber = string.Empty;
        }
    }
}

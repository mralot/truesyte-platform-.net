﻿using Com.TrueSyte.Platform.Core.Entities;
using System.ComponentModel.DataAnnotations;

namespace Com.TrueSyte.Platform.Core.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RegisterUserModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail address")]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = true)]
        [Display(Name = "Middle name")]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Company")]
        public string Company { get; set; }

        [Display(Name = "Department")]
        public string Department { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Work Number")]
        public string WorkNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Display(Name = "Credit card type")]
        public CreditCardType CardType { get; set; }

        [DataType(DataType.CreditCard)]
        [Display(Name = "Credit card number")]
        public string CardNumber { get; set; }

        [Display(Name = "Security Code")]
        public int SecurityCode { get; set; }

        [Display(Name = "Name in Credit card")]
        public string CardName { get; set; }

        [Display(Name = "Expiration date (MM/YYYY)")]
        public string ExpireationDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RegisterUserModel()
        {
            CardName = string.Empty;
            CardNumber = string.Empty;
            Company = string.Empty;
            Department = string.Empty;
            EmailAddress = string.Empty;
            ExpireationDate = string.Empty;
            FirstName = string.Empty;
            WorkNumber = string.Empty;
            JobTitle = string.Empty;
            LastName = string.Empty;
            MiddleName = string.Empty;
            MobileNumber = string.Empty;
        }
    }
}

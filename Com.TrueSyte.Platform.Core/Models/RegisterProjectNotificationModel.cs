﻿using Postal;
using System.ComponentModel.DataAnnotations;

namespace Com.TrueSyte.Platform.Core.Models
{
    public class RegisterProjectNotificationModel : Email
    {
        /// <summary>
        /// 
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ReferenceID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Objectives { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Technologies { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Dependencies { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Budget { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string JobTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RegisterProjectNotificationModel()
            : base("ProjectNotification")
        {
            Budget = 0.0M;
            Company = string.Empty;
            Dependencies = string.Empty;
            Description = string.Empty;
            Duration = 0;
            EmailAddress = string.Empty;
            FirstName = string.Empty;
            JobTitle = string.Empty;
            LastName = string.Empty;
            Objectives = string.Empty;
            Purpose = string.Empty;
            ReferenceID = string.Empty;
            Subtitle = string.Empty;
            Technologies = string.Empty;
            Title = string.Empty;
            To = string.Empty;
        }
    }
}

﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core.Models
{
    using Com.TrueSyte.Platform.Core.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public class ProjectDashboardModel
    {
        /// <summary>
        /// 
        /// </summary>
        public IList<ProjectEntity> InPlanning { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IList<ProjectEntity> InBuild { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IList<ProjectEntity> InOperations { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IList<ProjectEntity> InArchive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ProjectDashboardModel()
        {
            this.InArchive = new List<ProjectEntity>();
            this.InBuild = new List<ProjectEntity>();
            this.InOperations = new List<ProjectEntity>();
            this.InPlanning = new List<ProjectEntity>();
        }
    }
}

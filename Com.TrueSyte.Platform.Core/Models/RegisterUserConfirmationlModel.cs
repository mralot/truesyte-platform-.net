﻿using Postal;
using System.ComponentModel.DataAnnotations;

namespace Com.TrueSyte.Platform.Core.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RegisterUserConfirmationlModel : Email
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "To")]
        public string To { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RegisterUserConfirmationlModel()
            : base("UserConfirmation")
        {
            FirstName = string.Empty;
            LastName = string.Empty;
            Password = string.Empty;
            To = string.Empty;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Com.TrueSyte.Platform.Core.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class LogOnModel
    {
        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "The {0} is not a valid e-mail address.")]
        [Display(Name = "E-mail address")]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}

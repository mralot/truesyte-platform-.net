﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core.Models
{
    using Com.TrueSyte.Platform.Core.Entities;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// 
    /// </summary>
    public class RegisterProjectModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Lead Expert")]
        public int LeadExpert { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Experts")]
        public IList<int> Experts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Project Title")]
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(AllowEmptyStrings = true)]
        [DataType(DataType.Password)]
        [Display(Name = "Subtitle")]
        public string Subtitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Purpose")]
        public string Purpose { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Objectives")]
        public string Objectives { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Technologies")]
        public string Technologies { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Dependencies")]
        public string Dependencies { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Budget")]
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        public decimal PlannedBudget { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataType(DataType.Currency)]
        [Display(Name = "Total Cost (Estimate)")]
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        public decimal EstimatedCost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataType(DataType.Currency)]
        [Display(Name = "Total Cost")]
        [DisplayFormat(DataFormatString = "{0:F2}", ApplyFormatInEditMode = true)]
        public decimal ActualCost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Project Phase")]
        public ProjectPhase Phase { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Project Status")]
        public ProjectStatus Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [UIHint("DatePickerTemplate")]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date (Planned)")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PlannedStartDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [UIHint("DatePickerTemplate")]
        [DataType(DataType.Date)]
        [Display(Name = "End Date (Planned)")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PlannedEndDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UIHint("DatePickerTemplate")]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date (Estimate)")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EstimatedStartDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UIHint("DatePickerTemplate")]
        [DataType(DataType.Date)]
        [Display(Name = "End Date (Estimate)")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EstimatedEndDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UIHint("DatePickerTemplate")]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ActualStartDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UIHint("DatePickerTemplate")]
        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ActualEndDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RegisterProjectModel()
        {
            DateTime currentDate = DateTime.UtcNow;

            ActualEndDate = currentDate;
            ActualStartDate = currentDate;
            Dependencies = string.Empty;
            Description = string.Empty;
            EstimatedCost = 0.00M;
            EstimatedEndDate = currentDate;
            EstimatedStartDate = currentDate;
            Objectives = string.Empty;
            Phase = ProjectPhase.Initiation;
            PlannedBudget = 0.00M;
            PlannedEndDate = currentDate;
            PlannedStartDate = currentDate;
            Purpose = string.Empty;
            Status = ProjectStatus.Draft;
            Subtitle = string.Empty;
            Title = string.Empty;
        }
    }
}

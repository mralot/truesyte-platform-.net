﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;

    /// <summary>
    /// 
    /// </summary>
    public static class CryptoService
    {
        /// <summary>
        /// 
        /// </summary>
        private const int saltSize = 128;

        /// <summary>
        /// 
        /// </summary>
        private const int subKeyLength = 256;

        /// <summary>
        /// 
        /// </summary>
        private const int iterations = 1000;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static ITokenKey EncryptPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("password");
            }

            byte[] salt;
            byte[] subKey;

            using (Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes(password, saltSize, iterations))
            {
                salt = deriveBytes.Salt;
                subKey = deriveBytes.GetBytes(iterations);
            }

            int length = (saltSize + subKeyLength + iterations) + 1;

            byte[] outputBytes = new byte[length];
            Buffer.BlockCopy(salt, 0, outputBytes, 0, saltSize);
            Buffer.BlockCopy(subKey, 0, outputBytes, saltSize + 1, iterations);

            string encryptedPassword = Convert.ToBase64String(outputBytes);

            TokenKey tokenKey = new TokenKey()
            {
                Password = encryptedPassword,
                Salt = salt
            };

            return tokenKey;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenKey"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool VerifyEncryptedPassword(ITokenKey tokenKey, string password)
        {
            if (tokenKey == null)
            {
                throw new ArgumentNullException("tokenKey");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("password");
            }

            string encryptedPassword = tokenKey.Password;
            byte[] salt = tokenKey.Salt;

            byte[] encryptedPasswordBytes = Convert.FromBase64String(encryptedPassword);
            int length = (saltSize + subKeyLength + iterations) + 1;

            if (!(encryptedPasswordBytes.Length == length))
            {
                return false;
            }

            byte[] outputBytes = new byte[length];
            Buffer.BlockCopy(salt, 0, outputBytes, 0, saltSize);
            using (Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes(password, salt, iterations))
            {
                byte[] generatedSubKey = deriveBytes.GetBytes(iterations);
                Buffer.BlockCopy(generatedSubKey, 0, outputBytes, saltSize + 1, iterations);
            }

            return encryptedPasswordBytes.SequenceEqual<byte>(outputBytes);
        }
    }
}

﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core
{
    using Com.TrueSyte.Platform.Core.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public interface IExpertRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expertId"></param>
        /// <returns></returns>
        ExpertEntity GetById(int expertId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ExpertEntity GetByName(string name);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<ExpertEntity> GetAll();
    }
}

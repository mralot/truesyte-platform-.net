﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core.Repositories
{
    using Com.TrueSyte.Platform.Core.Entities;
    using Com.TrueSyte.Platform.Core.PersistentStorage;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public sealed class ProjectRepository : IProjectRepository
    {
        /// <summary>
        /// 
        /// </summary>
        private TrueSyteData DataContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ProjectRepository()
        {
            DataContext = TrueSyteData.Instance;
        }

        public bool Add(UserEntity user, ProjectEntity project)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            int userId = user.UserId;
            DateTime createdDate = DateTime.UtcNow;

            project.User = ServiceLocator.Instance.GetService<IUserRepository>().GetById(userId);
            project.LeadExpert = ServiceLocator.Instance.GetService<IExpertRepository>().GetById(-1);
            project.CreatedDate = createdDate;
            project.ModifiedDate = createdDate;

            DataContext.Projects.Add(project);
            int rowCount = DataContext.SaveChanges();

            return (rowCount > 0);
        }


        public IEnumerable<ProjectEntity> GetAllForUser(UserEntity user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            int userId = user.UserId;

            IEnumerable<ProjectEntity> projects =
                from entity in DataContext.Projects
                where entity.User.UserId == userId
                select entity;

            return projects.OrderBy(entity => entity.PlannedEndDate);
        }

        public IEnumerable<ProjectEntity> GetAllForUser(UserEntity user, ProjectPhase phase, ProjectStatus status)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            int userId = user.UserId;

            IEnumerable<ProjectEntity> projects =
                from entity in DataContext.Projects
                where entity.User.UserId == userId &&
                      entity.Phase == phase &&
                      entity.Status == status
                select entity;

            return projects.OrderBy(entity => entity.PlannedEndDate);
        }


        public bool Edit(UserEntity user, ProjectEntity project)
        {
            throw new NotImplementedException();
        }
    }
}

﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core.Repositories
{
    using Com.TrueSyte.Platform.Core.Entities;
    using Com.TrueSyte.Platform.Core.PersistentStorage;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 
    /// </summary>
    public sealed class ExpertRepository : IExpertRepository
    {
        private TrueSyteData DataContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ExpertRepository()
        {
            DataContext = TrueSyteData.Instance;
        }

        public ExpertEntity GetById(int expertId)
        {
            IEnumerable<ExpertEntity> experts =
                from entity in DataContext.Experts
                where entity.ExpertId == expertId && entity.IsActive
                select entity;

            ExpertEntity expert = experts.FirstOrDefault();

            return expert;
        }

        public ExpertEntity GetByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            IEnumerable<ExpertEntity> experts =
                from entity in DataContext.Experts
                let firstName = entity.FirstName
                let middleName = entity.MiddleName
                let lastName = entity.LastName
                where (firstName.Contains(name) || middleName.Contains(name) || lastName.Contains(name)) && entity.IsActive
                select entity;

            ExpertEntity expert = experts.FirstOrDefault();

            return expert;
        }

        public IEnumerable<ExpertEntity> GetAll()
        {
            IEnumerable<ExpertEntity> experts =
                from entity in DataContext.Experts
                where entity.IsActive
                select entity;

            return experts;
        }
    }
}

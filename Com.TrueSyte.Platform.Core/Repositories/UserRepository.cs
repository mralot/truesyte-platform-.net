﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core.Repositories
{
    using Com.TrueSyte.Platform.Core.Entities;
    using Com.TrueSyte.Platform.Core.PersistentStorage;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// 
    /// </summary>
    public sealed class UserRepository : IUserRepository
    {
        /// <summary>
        /// 
        /// </summary>
        private TrueSyteData DataContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public UserRepository()
        {
            DataContext = TrueSyteData.Instance;
        }

        public bool Add(UserEntity user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            DataContext.Users.Add(user);

            int rowCount = DataContext.SaveChanges();

            return (rowCount > 0);
        }

        public UserEntity LogOn(string emailAddress, string password)
        {
            IEnumerable<UserEntity> users =
                from entity in DataContext.Users
                where entity.EmailAddress == emailAddress
                select entity;

            UserEntity user = users.FirstOrDefault();

            if (user == null)
            {
                user = new UserEntity();
            }
            else
            {
                ITokenKey tokenKey = new TokenKey()
                {
                    Password = user.Password,
                    Salt = Convert.FromBase64String(user.Salt)
                };

                bool isVerified = 
                    CryptoService.VerifyEncryptedPassword(tokenKey, password);

                if (isVerified)
                {
                    user.LastSuccessfulLogin = DateTime.UtcNow;
                    user.IsAuthenticated = true;

                    DataContext.Users.Attach(user);

                    var entry = DataContext.Entry<UserEntity>(user);
                    entry.Property(entity => entity.LastSuccessfulLogin).IsModified = true;

                    int rowCount = DataContext.SaveChanges();
                }
            }

            return user;
        }

        public UserEntity GetById(int userId)
        {
            if (userId < 1)
            {
                throw new ArgumentNullException("userId");
            }

            IEnumerable<UserEntity> users =
                from entity in DataContext.Users
                where entity.UserId == userId
                select entity;

            UserEntity user = users.FirstOrDefault();

            return user;
        }

        public UserEntity GetByEmail(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            IEnumerable<UserEntity> users =
                from entity in DataContext.Users
                where entity.EmailAddress == emailAddress
                select entity;

            UserEntity user = users.FirstOrDefault();

            return user;
        }

        public bool EditPassword(UserEntity user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            user.IsEmailVerified = true;
            user.LastPasswordChange = DateTime.UtcNow;

            DataContext.Users.Attach(user);

            var entry = DataContext.Entry<UserEntity>(user);
            entry.Property(entity => entity.IsEmailVerified).IsModified = true;
            entry.Property(entity => entity.LastPasswordChange).IsModified = true;
            entry.Property(entity => entity.Password).IsModified = true;
            entry.Property(entity => entity.Salt).IsModified = true;

            int rowCount = DataContext.SaveChanges();

            return (rowCount > 0);
        }

        public bool Edit(UserEntity user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            DataContext.Users.Attach(user);

            var entry = DataContext.Entry<UserEntity>(user);
            entry.Property(entity => entity.Birthday).IsModified = true;
            entry.Property(entity => entity.City).IsModified = true;
            entry.Property(entity => entity.Company).IsModified = true;
            entry.Property(entity => entity.Country).IsModified = true;
            entry.Property(entity => entity.Department).IsModified = true;
            entry.Property(entity => entity.FaxNumber).IsModified = true;
            entry.Property(entity => entity.FirstName).IsModified = true;
            entry.Property(entity => entity.HomeNumber).IsModified = true;
            entry.Property(entity => entity.IsEmailVerified).IsModified = true;
            entry.Property(entity => entity.JobTitle).IsModified = true;
            entry.Property(entity => entity.LastName).IsModified = true;
            entry.Property(entity => entity.MiddleName).IsModified = true;
            entry.Property(entity => entity.MobileNumber).IsModified = true;
            entry.Property(entity => entity.Nickname).IsModified = true;
            entry.Property(entity => entity.PagerNumber).IsModified = true;
            entry.Property(entity => entity.State).IsModified = true;
            entry.Property(entity => entity.Street).IsModified = true;
            entry.Property(entity => entity.VoIPNumber).IsModified = true;
            entry.Property(entity => entity.WorkNumber).IsModified = true;
            entry.Property(entity => entity.PostalCode).IsModified = true;

            int rowCount = DataContext.SaveChanges();

            return (rowCount > 0);
        }
    }
}

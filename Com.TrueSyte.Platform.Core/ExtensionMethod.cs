﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;
    using System.Web.Mvc;

    /// <summary>
    /// 
    /// </summary>
    public static class ExtensionMethod
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumeration"></param>
        /// <param name="selected"></param>
        /// <param name="defaultText"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> ToSelectList(this Type type, string defaultText)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            IList<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem()
            {
                Selected = true,
                Text = defaultText,
                Value = "0"
            });

            Type attributeType = typeof(DisplayAttribute);

            if (!type.IsEnum)
            {
                throw new ArgumentException("Enumeration type is expected.");
            }

            foreach (var element in Enum.GetValues(type))
            {
                int value = (int)element;
                string name = Enum.GetName(type, value);

                FieldInfo field = element.GetType().GetField(name);
                DisplayAttribute attributes =
                    (DisplayAttribute)field.GetCustomAttributes(attributeType, false).FirstOrDefault();

                string displayName = (attributes == null) ? name : attributes.GetName();

                items.Add(new SelectListItem()
                {
                    Selected = false,
                    Text = displayName,
                    Value = value.ToString()
                });
            }

            return items;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumeration"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> ToSelectList(this Type type)
        {
            return type.ToSelectList("Select ...");
        }
    }
}

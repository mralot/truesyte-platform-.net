﻿/****************************** Module Header ******************************\
Module Name:  UniqueConstraintException.cs
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core.PersistentStorage
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    public class UniqueConstraintException : Exception, ISerializable
    {
        /// <summary>
        /// 
        /// </summary>
        public UniqueConstraintException() :
            base()
        {
            // No logic
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public UniqueConstraintException(string message) :
            base(message)
        {
            // No logic
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public UniqueConstraintException(string message, Exception innerException) :
            base(message, innerException)
        {
            // No logic
        }
    }
}

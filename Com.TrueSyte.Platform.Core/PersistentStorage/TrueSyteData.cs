﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core.PersistentStorage
{
    using Com.TrueSyte.Platform.Core.Entities;
    using System.Configuration;
    using System.Data.Entity;

    /// <summary>
    /// 
    /// </summary>
    public sealed class TrueSyteData : DbContext
    {
        private static volatile TrueSyteData _instance;
        private static object _padlock = new object();

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ExpertEntity> Experts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ProjectEntity> Projects { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<UserEntity> Users { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static TrueSyteData Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_padlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new TrueSyteData();
                        }
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        private TrueSyteData(bool mars = false)
            : base("Default")
        {
            this.Database
                .Connection
                .ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString +
                                    (mars ? ";MultipleActiveResultSets=true;" : string.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ProjectEntity>()
                        .HasMany<ExpertEntity>(project => project.Experts)
                        .WithMany();
        }
    }
}

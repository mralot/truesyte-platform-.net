﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core
{
    using Com.TrueSyte.Platform.Core.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public interface IProjectRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        bool Add(UserEntity user, ProjectEntity project);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        IEnumerable<ProjectEntity> GetAllForUser(UserEntity user);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="phase"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<ProjectEntity> GetAllForUser(UserEntity user, ProjectPhase phase, ProjectStatus status);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        bool Edit(UserEntity user, ProjectEntity project);
    }
}

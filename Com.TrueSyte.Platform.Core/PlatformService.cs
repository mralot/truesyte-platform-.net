﻿/****************************** Module Header ******************************\
Module Name:  
Project:      Com.TrueSyte.Platform.Core
Copyright (c) Microsoft Corporation.

<Description of the file>

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/

namespace Com.TrueSyte.Platform.Core
{
    using Com.TrueSyte.Platform.Core.Entities;
    using Com.TrueSyte.Platform.Core.Models;
    using Com.TrueSyte.Platform.Core.PersistentStorage;
    using Postal;
    using System;
    using System.Data.SqlClient;

    /// <summary>
    /// 
    /// </summary>
    public class PlatformService : IPlatformService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public UserEntity LogOn(LogOnModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            UserEntity user = 
                ServiceLocator.Instance.GetService<IUserRepository>().LogOn(model.EmailAddress, model.Password);

            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public ManageUserModel GetUserDetails(string emailAddress)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            UserEntity user =
                ServiceLocator.Instance.GetService<IUserRepository>().GetByEmail(emailAddress);

            ManageUserModel model = new ManageUserModel()
            {
                Birthday = user.Birthday,
                City = user.City,
                Company = user.Company,
                Country = user.Country,
                Department = user.Department,
                FaxNumber = user.FaxNumber,
                FirstName = user.FirstName,
                HomeNumber = user.HomeNumber,
                JobTitle = user.JobTitle,
                LastName = user.LastName,
                MiddleName = user.MiddleName,
                MobileNumber = user.MobileNumber,
                PagerNumber = user.PagerNumber,
                PostalCode = user.PostalCode,
                State = user.State,
                Street = user.Street,
                VoIPNumber = user.VoIPNumber,
                WorkNumber = user.WorkNumber
            };

            return model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool RegisterUser(RegisterUserModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            string password = DateTime.UtcNow.GetHashCode().ToString();
            ITokenKey tokenKey = CryptoService.EncryptPassword(password);
            string salt = Convert.ToBase64String(tokenKey.Salt);

            UserEntity user = new UserEntity()
            {
                Company = model.Company,
                Department = model.Department,
                EmailAddress = model.EmailAddress,
                FirstName = model.FirstName,
                JobTitle = model.JobTitle,
                LastName = model.LastName,
                MiddleName = model.MiddleName,
                MobileNumber = model.MobileNumber,
                Password = tokenKey.Password,
                Salt = salt,
                WorkNumber = model.WorkNumber
            };

            bool result = false;

            try
            {
                result = 
                    ServiceLocator.Instance.GetService<IUserRepository>().Add(user);

                if (result)
                {
                    RegisterUserConfirmationlModel email = new RegisterUserConfirmationlModel()
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Password = password,
                        To = user.EmailAddress
                    };

                    email.Send();
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    string messageFormat = 
                        "The e-mail address ({0}) you provided already exists in our system. Please choose another e-mail address to register.";
                    string message = string.Format(messageFormat, user.EmailAddress);

                    throw new UniqueConstraintException(message);
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ManageUser(string emailAddress, ManageUserModel model)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            UserEntity user = 
                ServiceLocator.Instance.GetService<IUserRepository>().GetByEmail(emailAddress);

            user.Birthday = model.Birthday;
            user.City = model.City;
            user.Company = model.Company;
            user.Country = model.Country;
            user.Department = model.Department;
            user.FaxNumber = model.FaxNumber;
            user.FirstName = model.FirstName;
            user.HomeNumber = model.HomeNumber;
            user.JobTitle = model.JobTitle;
            user.LastName = model.LastName;
            user.MiddleName = model.MiddleName;
            user.MobileNumber = model.MobileNumber;
            user.PagerNumber = model.PagerNumber;
            user.PostalCode = model.PostalCode;
            user.State = model.State;
            user.Street = model.Street;
            user.VoIPNumber = model.VoIPNumber;
            user.WorkNumber = model.WorkNumber;

            bool result = 
                ServiceLocator.Instance.GetService<IUserRepository>().Edit(user);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ManagePassword(string emailAddress, ManagePasswordModel model)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            bool result = false;

            UserEntity user = 
                ServiceLocator.Instance.GetService<IUserRepository>().GetByEmail(emailAddress);

            ITokenKey tokenKey = new TokenKey() 
            {
                Password = user.Password,
                Salt = Convert.FromBase64String(user.Salt)
            };

            bool isVerified = 
                CryptoService.VerifyEncryptedPassword(tokenKey, model.CurrentPassword);

            if (isVerified)
            {
                tokenKey = CryptoService.EncryptPassword(model.NewPassword);
                string salt = Convert.ToBase64String(tokenKey.Salt);

                user.Password = tokenKey.Password;
                user.Salt = salt;

                result =
                    ServiceLocator.Instance.GetService<IUserRepository>().EditPassword(user);

                if (result)
                {
                    // TODO: Send e-mail to the user notifying the user that the password has been changed.
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool RegisterProject(string emailAddress, RegisterProjectModel model)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            UserEntity user = 
                ServiceLocator.Instance.GetService<IUserRepository>().GetByEmail(emailAddress);

            ProjectEntity project = new ProjectEntity()
            {
                Dependencies = model.Dependencies,
                Description = model.Description,
                Objectives = model.Objectives,
                Phase = model.Phase,
                PlannedBudget = model.PlannedBudget,
                PlannedEndDate = model.PlannedEndDate.Value,
                PlannedStartDate = model.PlannedStartDate.Value,
                Purpose = model.Purpose,
                Status = model.Status,
                Subtitle = model.Subtitle,
                Technologies = model.Technologies,
                Title = model.Title
            };

            bool result = 
                ServiceLocator.Instance.GetService<IProjectRepository>().Add(user, project);

            if (result)
            {
                // TODO: Send a confirmation e-mail to the user with project details
                // TODO: Send e-mail to all Lead Experts of the proposed technology set
            }

            return result;
        }
    }
}

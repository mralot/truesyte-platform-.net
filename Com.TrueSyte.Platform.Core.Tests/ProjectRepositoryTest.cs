﻿using Com.TrueSyte.Platform.Core.Entities;
using Com.TrueSyte.Platform.Core.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Com.TrueSyte.Platform.Core.Tests
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class ProjectRepositoryTest
    {
        /// <summary>
        /// 
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ProjectRepositoryTest()
        {
            // No logic
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AddProjectTest()
        {
            UserEntity user = new UserEntity()
            {
                UserId = 1
            };

            DateTime startDate = DateTime.Now.AddDays(7);
            DateTime endDate = startDate.AddMonths(6);

            ProjectEntity projectEntity = new ProjectEntity()
            {
                Title = "Accenture.com Platform Upgrade to SharePoint 2013", 
                Subtitle = "Upgrade from SharePoint 2007 to SharePoint 2013",
                Description = "Upgrade from SharePoint 2007 to SharePoint 2013", 
                Purpose = "The upgrade is to show case the technologies being used by Accenture.", 
                Objectives = "To leverage the out-of-the-box (OOB) components and to ease maintainance of the web application.", 
                Technologies = "SharePoint 2013, .NET 4.5 Framework, SQL Server 2012", 
                Dependencies = "Microsoft Consultants in the United States", 
                PlannedBudget = 1000000.00M, 
                PlannedStartDate = startDate,
                PlannedEndDate = endDate,
                Phase = ProjectPhase.Initiation,
                Status = ProjectStatus.Draft
            };

            IProjectRepository Repository = new ProjectRepository();
            bool isSuccessful = Repository.Add(user, projectEntity);

            Assert.IsTrue(isSuccessful);
        }

    }
}

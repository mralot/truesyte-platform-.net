﻿using Com.TrueSyte.Platform.Core.Entities;
using Com.TrueSyte.Platform.Core.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Com.TrueSyte.Platform.Core.Tests
{
    [TestClass]
    public class UserRepositoryTest
    {
        /// <summary>
        /// 
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public UserRepositoryTest()
        {
            // No logic
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AddUserTest()
        {
            string password = DateTime.UtcNow.GetHashCode().ToString();
            ITokenKey tokenKey = CryptoService.EncryptPassword(password);
            string salt = Convert.ToBase64String(tokenKey.Salt);

            UserEntity user = new UserEntity()
            {
                Company = "HSBC",
                Department = "GPS",
                EmailAddress = "Mark Ryan Villagracia ALOT/HDPP/HSBC",
                FaxNumber = "",
                FirstName = "Mark Ryan",
                HomeNumber = "+639464340991",
                JobTitle = "WDL",
                LastName = "Alot",
                MiddleName = "Villagracia",
                MobileNumber = "+63915869149",
                PagerNumber = "",
                Password = password,
                Salt = salt,
                VoIPNumber = "",
                WorkNumber = ""
            };

            IUserRepository Repository = new UserRepository();
            bool isSuccessful = Repository.Add(user);

            Assert.IsTrue(isSuccessful);
        }

        [TestMethod]
        public void AuthenticateUserTest()
        {
            string emailAddress = "mralot@gmail.com";
            string password = "P@ssw0rd";

            IUserRepository Repository = new UserRepository();
            UserEntity user = Repository.LogOn(emailAddress, password);

            Assert.AreEqual<string>(emailAddress, user.EmailAddress);
        }
    }
}

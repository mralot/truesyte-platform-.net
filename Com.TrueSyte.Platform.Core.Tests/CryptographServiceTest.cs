﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Com.TrueSyte.Platform.Core.Tests
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class CryptographServiceTest
    {
        /// <summary>
        /// 
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CryptographServiceTest()
        {
            // No logic
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void EncryptPasswordTest()
        {
            string password = "P@ssw0rd";
            ITokenKey tokenKey = CryptoService.EncryptPassword(password);

            Assert.AreEqual<int>(1848, tokenKey.Password.Length);
        }

        [TestMethod]
        public void VerifyEncryptedPasswordTest()
        {
            string password = "P@ssw0rd";
            string encryptedPassword =
                "A93FNqbno8HUYHTVAavFbAL3Sc+oAySDMxw8r1acGvcjNFRwaT8UPxvHOyH6FmEw41YYZs65ha4rcpVya7a3uBIJfn1Pj8LH7L3cVkcHojozSv999cVXceBGIFOIST3NjB09ZGxD80wVfS2tGcsguzkHlHNm9YdW/+p3rVcH5EwAwum1mCyNcQNb2eCs2CkolYHXuUzpZXzmukKW/DFCqNq4iWE/ixa2hLjllMqWTtnOcck3lP9ibNRR5RYrpDd+T1CHuASoBNVXPeREnHbk19oZ2N0dYs63V9lUyqa290x6tRokVuPhwNVMt3bzmCeoBGHevLPBfF4fKzAqtx4366woKXFkEZyfea5u1qIqCRP3fsF5LIoCGAePjUZapGgeqRuBZ2cglGuzLZLq53iS609SpRgpsXLZX9cmDHXXXBPaA8O2j9AvK7Y0uFmglktjkGGOK0tTe+7ytqngw7xBbl50IrQkrnmsdm7aViHAyNOQDVRgZ25eN6KX6Fgi5YpVagtuEm5nlt5KOzOlxwjZ3uVHSl3qVW9u1r8lZLxqwBF1D6d4JXGLxoeiu/8u9vq0P0MHAxZcXuaqof5k7qLSvYZyWvUwwJ9BuTuEMyKY5XBXOBxTs96mZxRTopRTsfrfxyS9ciJBZL9nbNNOPad9n75I30jSHyHORHyMiMMGDx0eYau/79WjGzmsjT5bRbj05x7HxzXJiyUgnX2xxGphhHYpsh1nl/4B2xIEdp3aYi/KDlSSc7bJZvSVweRTiUwHiRd/tXA3TniPji3Fevg4QRMb8drxRs6u7o26G3be/33+6Yx4lr7Va6mu/IS888YBhcHDR5MMvJUAAbu+XxyBxLDWLg742yLzq2osGB/4dFS58D++I+nrQl057GJkgzfeuvNnx5Z+U2CyhWWgS4L8KeFczlLsG7AeUz6VsJPu4JD3Rf8Gl0ytXsMfJ7o+FnOuQtwE3EoL0yOiWXP20DU9D0iF8iCOuIG6mCKBBYhHJThAlXuy+eh9YcR2gXF7oSWR2SaBNwpY0XYJp1eU/41++vvCOuff8beO5GjadcDo3fexUuQEsbHp1L0o3dk0N7ju36APpA+uuQdRIDw2Zx1S952VvzvRUqwUAYrYKy67zj/32S1IZQ9I1VmQwcJ74KmFRYoqh6yCJGgCAok7qltoVhf1b9sgUQ7YuHKFcxMvVJNPwEfyfyGsCQS0F1+RXfBpGFSRqOqzamHsFY/x/nGBGCR7qaFJ5g+HO5jn/IyxsL96+8xNViMpfdcyTnf3UX0P212EuFYLd7ifI6LpHE/7EDT3zLtyD5ylndiuFOg/ymdI1W+/3xXf7JAVjQ77/0Rpn2GolYSZfVe8C1hFbKfxkcfU97wjGgQrrlnync9ubLbPevWf2bxMaZWeg+tkRLFcQ3HDLrsFur+g571WYSFv/BGDHfSCk10nil7PYheQ1Jd5h45FtH6kKgAQXCLOjHuIVEia0L48gtF5r/XQHtkgH4rkRto6fJpS7wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
            string passwordSalt = "A93FNqbno8HUYHTVAavFbAL3Sc+oAySDMxw8r1acGvcjNFRwaT8UPxvHOyH6FmEw41YYZs65ha4rcpVya7a3uBIJfn1Pj8LH7L3cVkcHojozSv999cVXceBGIFOIST3NjB09ZGxD80wVfS2tGcsguzkHlHNm9YdW/+p3rVcH5Ew=";

            byte[] salt = Convert.FromBase64String(passwordSalt);

            ITokenKey tokenKey = new TokenKey()
            {
                Password = encryptedPassword,
                Salt = salt
            };

            bool isVerified = CryptoService.VerifyEncryptedPassword(tokenKey, password);

            Assert.IsTrue(isVerified);
        }
    }
}

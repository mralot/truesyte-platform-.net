﻿using Com.TrueSyte.Platform.Core.Entities;
using Com.TrueSyte.Platform.Core.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Com.TrueSyte.Platform.Core.Tests
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class ExpertRepositoryTest
    {
        /// <summary>
        /// 
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ExpertRepositoryTest()
        {
            // No logic
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void GetAllTest()
        {
            IExpertRepository Repository = new ExpertRepository();
            IEnumerable<ExpertEntity> experts = Repository.GetAll();

            Assert.IsTrue(experts != null);
        }

        [TestMethod]
        public void GetByIdTest()
        {
            int expertId = 1;

            IExpertRepository Repository = new ExpertRepository();
            ExpertEntity expert = Repository.GetById(expertId);

            Assert.AreEqual<int>(expertId, expert.ExpertId);
        }

        [TestMethod]
        public void GetByNameTest()
        {
            string name = "Mark Ryan";

            IExpertRepository Repository = new ExpertRepository();
            ExpertEntity expert = Repository.GetByName(name);

            Assert.AreEqual<string>(name, expert.FirstName);
        }

    }
}

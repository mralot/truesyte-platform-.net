﻿using Com.TrueSyte.Platform.Core.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Com.TrueSyte.Platform.Core.Tests
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class PlatformExtensionTest
    {
        /// <summary>
        /// 
        /// </summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PlatformExtensionTest()
        {
            // No logic
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void ToSelectListTest()
        {
            IEnumerable<SelectListItem> items = typeof(ProjectPhase).ToSelectList("");

            IList<SelectListItem> list = items.ToList();

            Assert.AreEqual<string>("0", list[0].Value);
        }
    }
}
